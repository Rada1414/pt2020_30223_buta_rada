import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import  javax.swing.border.*;

public class PolinomPanel
{     JPanel mainPanel;
	  JPanel polin1,polin2;
	  JLabel showLabel1,showLabel2,showLabel3,showLabel4,showLabel5,showLabel6;
	  JLabel wLabel1,wLabel2,wLabel3,wLabel4,wLabel5,wLabel6;
	  JButton add,substract,multiply,divide;
	  JTextField text1,text2;
	  JPanel signs,  showPanel1,showPanel2,showPanel3,showPanel4,showPanel5,showPanel6;
	   JFrame frame=new JFrame("Polinomial Calculator");
	   
	  
	     public PolinomPanel()
	     {
	    	 buildFrame();
	     }
	     
	     private void buildFrame() {
	     
	    	mainPanel=new JPanel();
	    	 mainPanel.setBackground(Color.black);
	    	 mainPanel.setSize(800,500);
	    	 mainPanel.setLayout(new GridLayout(3,3));
	    	 mainPanel.setBorder(BorderFactory.createMatteBorder(4,4,4,4,Color.PINK));
	    	 
	    	 
	    	 frame.setSize(1000,1000);
	    	 frame.setBackground(Color.BLUE);
	    	 frame.setLayout(new GridLayout());
	    	 frame.add(mainPanel);
	    	
	        
	    	 
	          polin1=new JPanel();
	          polin1.setLayout(new GridLayout(4,4));
	          JLabel p1=new JLabel("Primul polinom:",JLabel.CENTER);
	          p1.setSize(new Dimension(100,100));
	          p1.setBackground(Color.BLUE);
	          text1=new JTextField(20);
	          text1.setBackground(Color.CYAN);
	          text1.setSize(new Dimension(40,40));
	          p1.setSize(new Dimension(100,100));
	          polin1.add(p1);
	          polin1.add(text1);
		      polin1.setBorder(BorderFactory.createMatteBorder(4,4,4,4,Color.BLUE));
		      polin1.setBackground(Color.PINK);
		         
		         
	          polin2=new JPanel();
	          polin2.setLayout(new GridLayout(4,4));
	          JLabel p2=new JLabel("Polinomul al doilea:",JLabel.CENTER);
	          p2.setSize(new Dimension(100,100));
	          p2.setBackground(Color.BLUE);
	          text2=new JTextField(20);
	          text2.setBackground(Color.CYAN);
	          text2.setSize(new Dimension(40,40));
	           p2.setSize(new Dimension(100,100));
	           polin2.add(p2);
	           polin2.setBorder(BorderFactory.createMatteBorder(4,4,4,4,Color.BLUE));
	           polin2.add(text2);
	           polin2.setBackground(Color.pink);
	           
	           frame.add(polin1);
	           frame.add(polin2);
	           
	           
	        showLabel1=new JLabel("Polinomul 1 este: ",JLabel.LEFT);
	     	showLabel1.setPreferredSize(new Dimension(200,20));
	        wLabel1=new JLabel("",JLabel.LEFT);
	        wLabel1.setVerticalTextPosition(JLabel.CENTER);
	        wLabel1.setPreferredSize(new Dimension(200,20));
	        showPanel1=new JPanel();
	     	showPanel1.setLayout(new GridLayout(8,2));
	        showPanel1.add(showLabel1);
	     	showPanel1.add(wLabel1);
	    	showPanel1.setBorder(BorderFactory.createMatteBorder(3,3,3,3,Color.blue));
	     	showPanel1.setBackground(Color.cyan);
	     
	     	showLabel2=new JLabel("Polinomul 2 este : ",JLabel.LEFT);
	     	showLabel2.setPreferredSize(new Dimension(200,20));
	        wLabel2=new JLabel("",JLabel.LEFT);
	        wLabel2.setVerticalTextPosition(JLabel.TOP);
	        wLabel2.setPreferredSize(new Dimension(200,20));
	     	JPanel showPanel2=new JPanel();
	     	showPanel2.setLayout(new GridLayout(8,2));
	     	showPanel2.add(showLabel2);
	     	showPanel2.add(wLabel2);
	    	showPanel2.setBorder(BorderFactory.createMatteBorder(3,3,3,3,Color.blue));
	     	showPanel2.setBackground(Color.cyan);
	      
	     	showLabel3=new JLabel("Rezultatul scaderii:",JLabel.LEFT);
	     	showLabel3.setPreferredSize(new Dimension(200,20));
	     wLabel3=new JLabel("",JLabel.LEFT);
	     	wLabel3.setVerticalTextPosition(JLabel.TOP);
	     	wLabel3.setPreferredSize(new Dimension(200,20));
	     	 showPanel3=new JPanel();
	     	showPanel3.setLayout(new GridLayout(8,2));
	     	showPanel3.add(showLabel3);
	     	showPanel3.add(wLabel3) ;	
	    	showPanel3.setBorder(BorderFactory.createMatteBorder(3,3,3,3,Color.blue));
	     	showPanel3.setBackground(Color.cyan);
	     showLabel4=new JLabel("Rezultatul impartirii:",JLabel.LEFT);
	     	showLabel4.setPreferredSize(new Dimension(200,20));
	         wLabel4=new JLabel("",JLabel.LEFT);
	     	wLabel4.setVerticalTextPosition(JLabel.TOP);
	     	wLabel4.setPreferredSize(new Dimension(200,20));
	     showPanel4=new JPanel();
	     	showPanel4.setLayout(new GridLayout(8,2));
	     	showPanel4.add(showLabel4);
	     	showPanel4.add(wLabel4);
	     	showPanel4.setBorder(BorderFactory.createMatteBorder(3,3,3,3,Color.blue));
	     	showPanel4.setBackground(Color.cyan);
	     	showLabel5=new JLabel("Rezultatul adunarii :",JLabel.LEFT);
	     	showLabel5.setPreferredSize(new Dimension(200,20));
	      wLabel5=new JLabel("",JLabel.LEFT);
	     	wLabel5.setVerticalTextPosition(JLabel.TOP);
	     	wLabel5.setPreferredSize(new Dimension(200,20));
	     	 showPanel5=new JPanel();
	     	showPanel5.setLayout(new GridLayout(8,2));
	     	showPanel5.add(showLabel5);
	     	showPanel5.add(wLabel5);
	     	showPanel5.setBorder(BorderFactory.createMatteBorder(3,3,3,3,Color.blue));
	    	showPanel5.setBackground(Color.cyan);
	     	
	    showLabel6=new JLabel("Rezultatul inmultirii: ",JLabel.LEFT);
	     	showLabel6.setPreferredSize(new Dimension(200,20));
	      wLabel6=new JLabel("",JLabel.LEFT);
	     	wLabel6.setVerticalTextPosition(JLabel.TOP);
	     	wLabel6.setPreferredSize(new Dimension(200,20));
	     showPanel6=new JPanel();
	     	showPanel6.setLayout(new GridLayout(8,2));
	     	showPanel6.add(showLabel6);
	     	showPanel6.add(wLabel6);
	     	showPanel6.setBorder(BorderFactory.createMatteBorder(3,3,3,3,Color.blue));
	     	showPanel6.setBackground(Color.cyan);
	     	
	         
	     	signs=new JPanel();
	     	signs.setLayout(new GridLayout(4,4));
	     	
	     	add=PolinomPanel.makeButton("Add");
	     	substract=PolinomPanel.makeButton("Substract");
	     	multiply=PolinomPanel.makeButton("Multiply");
	     	divide=PolinomPanel.makeButton("Divide");
	     	signs.add(add);
	     	signs.add(substract);
	     	signs.add(multiply);
	     	signs.add(divide);
	     	
	     	mainPanel.add(polin1);
	     	mainPanel.add(polin2);
	     	mainPanel.add(signs);
	     	mainPanel.add(showPanel1);
	    	mainPanel.add(showPanel2);
	    	mainPanel.add(showPanel3);
	    	mainPanel.add(showPanel4);
	    	mainPanel.add(showPanel5);
	    	mainPanel.add(showPanel6);
	    	mainPanel.setVisible(true);
	    
	     	
	     }
	    	private static JButton makeButton(String text)
	    	{
	    		JButton button=new JButton();
	    		button.setText(text);
	    		button.setForeground(Color.BLACK);
	    		button.setBackground(Color.PINK);
	    		Border b1=BorderFactory.createLineBorder(Color.cyan);
	    		Border b2 =new EmptyBorder(5,15,5,15);
	    		Border b3=new CompoundBorder(b1,b2);
	    		button.setBorder(b3);
	    		return button;
	    	}
	    	
	    	 
	    	 public void showEventDemo(){ 
	    	        JButton button1 = new JButton("POLINOM1_OK");
	    	        JButton button2 =  new JButton("POLINOM2_OK");
	    	        button1.setActionCommand("POLINOM1_OK");
	    	        button2.setActionCommand("POLINOM2_OK");
	    	        add.setActionCommand("add");
	    	        substract.setActionCommand("substract");
	    	        multiply.setActionCommand("multiply");
	    	        divide.setActionCommand("divide");
	    	
	    	        button1.addActionListener(new ReadPolinom(this));
	    	        button2.addActionListener(new ReadPolinom(this));
	    	        add.addActionListener(new ReadPolinom (this));
	    	        substract.addActionListener(new ReadPolinom(this));
	    	        multiply.addActionListener(new ReadPolinom(this));
	    	        divide.addActionListener(new ReadPolinom(this));
	    	      
	    	         
	    	        polin1.add(button1);
	    	        polin2.add(button2);
	    	        
	    	        frame.setVisible(true);
	    	    }
	     


}