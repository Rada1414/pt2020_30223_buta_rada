import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ReadPolinom implements ActionListener{

		PolinomPanel panel;
		String[] monoame1;
		String[] monoame2;
		
		public ReadPolinom(PolinomPanel panel) {
			this.panel = panel;
		}
		
	    public void actionPerformed(ActionEvent e) {
	      
	        
	        if(e.getActionCommand().equals("POLINOM1_OK"))  {
	            String txt = panel.text1.getText();
	        	Polinom poli = this.buildPol(txt);
	    		
	        	panel.showLabel1.setText("Rezultat polinom1:"+  poli.getRez() );
	        	panel.wLabel1.setText("Polinom 1:" + poli.show() );
	        } else if(e.getActionCommand().equals("POLINOM2_OK")) {
	            String txt = panel.text2.getText();
	        	Polinom poli =  this.buildPol(txt);
	        	panel.showLabel2.setText("Rezultat polinom 2: " + poli.getRez()  );
	        	panel.wLabel2.setText("Polinom 2: " + poli.show() );
	        } else if(e.getActionCommand().equals("add")) {
	            String txt1 = panel.text1.getText();
	            String txt2 = panel.text2.getText();
	        	Polinom poli3 = this.add(txt1, txt2);
	        	panel.showLabel5.setText("Rezultat adunare polinoame: "+ poli3.getRez() );
	        	panel.wLabel5.setText("Polinom 3: " + poli3.show() );
	        } else if(e.getActionCommand().equals("substract")) {
	            String txt1 = panel.text1.getText();
	            String txt2 = panel.text2.getText();
	        	Polinom poli3 = this.sub(txt1, txt2);
	        	panel.showLabel3.setText(" Rezultat scadere polinoame: "+ poli3.getRez() );
	        	panel.wLabel3.setText("Polinom 4:" + poli3.show() );
	        } else if(e.getActionCommand().equals("multiply")) {
	            String txt1 = panel.text1.getText();
	            String txt2 = panel.text2.getText();
	        	Polinom poli3 = this.multiply(txt1, txt2);
	        	panel.showLabel6.setText("Rezultat inmultire polinoame: "+ poli3.getRez() );
	        	panel.wLabel6.setText("Polinom 5: " + poli3.show() );
	        } else if(e.getActionCommand().equals("divide")) {
	        	String txt1 = panel.text1.getText();
	            String txt2 = panel.text2.getText();
	            ArrayList<Polinom> polis = new ArrayList<Polinom>();
	            polis = this.divide(txt1, txt2);
	            panel.showLabel4.setText("Rezultat impartire polinoame: " + polis.get(0).getRez() + "   |   " + polis.get(1).getRez() );
	            panel.wLabel4.setText("Polinom 6: " + polis.get(0).show() + "  |   " + polis.get(1).show() );
	         }
	    }
	    
	    private Polinom buildPol(String polinom) {
	    	String monoame[];
	    	String readyP;

	    	
	    		String actualBuffer = (polinom.contains("-")) ? polinom.replace("-", "+-") : polinom;
	        	readyP = (actualBuffer.charAt(0) == '+') ? actualBuffer.substring(1) : actualBuffer;
	        	monoame = readyP.split("\\+"); 
	        	Polinom p= new Polinom(monoame);
	        	return p;
	    	
	    }
	    
	    private Polinom add(String txt1, String txt2) {
	    	Polinom p1, p2, p3;
	    	p1 = buildPol(txt1);
	    	p2 = buildPol(txt2);
	    	p3 = p1.add(p2);
	    	return p3;
	    }
	    
	    private Polinom sub(String txt1, String txt2) {
	    	Polinom p1, p2, p3;
	    	p1 = buildPol(txt1);
	    	p2 = buildPol(txt2);
	    	p3 = p1.substract(p2);
	    	return p3;
	    }
	    
	    private Polinom multiply(String txt1, String txt2) {
	    	Polinom p1, p2, p3;
	    	p1 = buildPol(txt1);
	    	p2 = buildPol(txt2);
	    	p3 = p1.multiply(p2);
	    	return p3;
	    }
	    
	    private ArrayList<Polinom> divide(String txt1, String txt2) {
	    	Polinom p1, p2;
	    	ArrayList<Polinom> p3 = new ArrayList<Polinom>();
	    	p1 = buildPol(txt1);
	    	p2 = buildPol(txt2);
	    	p3 = p1.divide(p2);
	    	return p3;
	    }
		    
	}

	


