import java.util.*; 



public class Polinom {
	public int x;
	ArrayList<Monom> m = new ArrayList<Monom>();
	String[] monoame;
	
	

	
	private Polinom(Monom m, int x) {
		this.m.add(m);
		this.x = x;
		
	}
	
	public String show() {
		String showedPol = new String(m.get(0).getMonom());
		for(int i=1;i<m.size();i++) {
		showedPol+= "+" + m.get(i).getMonom();		
			}
		return showedPol;
	}
	
	public Polinom(String[] monoame1) {
		this.monoame = monoame1;
	
		for(int i=0; i<monoame1.length; i++) {
			System.out.println(monoame1[i]);
			m.add(new Monom(monoame[i]));
		}
	}
	
	private Polinom(int x) {
		this.x = x;
	}
	
	

	private int getIndExp(int exp) {
		for(int i=0;i<m.size();i++) {
			Monom mon = m.get(i);
			if(exp == mon.getExponent()) 
				return i;
		}
		return -1;
	}
	private void delete() {
		for(int i=0;i<m.size();i++) {
			if(m.get(i).coef== 0) m.remove(i);//daca are coeficientul 0 atunci se sterge direct monomul 
		}
	}
	

	
	public double getRez() {
		double s = 0;
		for(int i=0;i<m.size(); i++) {
			s+=m.get(i).rez(x);
		}
		return s;
	}
	
	public int getGrad() {
		int expMax=-999; 
		for(int i=0;i<m.size();i++) {
			if(m.get(i).getExponent() > expMax) 
				expMax = m.get(i).getExponent();
		}
		return expMax;
	}
	

	private Monom findMax(Polinom p1) {
		int putere = p1.getGrad();
		int iP = p1.getIndExp(putere);
		return p1.m.get(iP);
	}
	
        public Polinom add(Polinom p2) {
	
		Polinom p3 = new Polinom(x);
		
		for(int i=0;i<this.m.size();i++) {
		
			int p1 = this.m.get(i).getExponent();
			int c1 = this.m.get(i).getCoeficient();
			
			int iP = p2.getIndExp(p1);
			if(iP != -1) {
				int e3 = p2.m.get(i).getExponent();
				int c3 = p2.m.get(iP).getCoeficient();
				p3.m.add(new Monom(c1+c3, e3));
			
				p2.m.remove(iP);
			
			} else {
			
				p3.m.add(new Monom(c1, p1));
			}
		}
	
		for(int i=0;i<p2.m.size();i++) {
			int e2 = p2.m.get(i).getExponent();
			int c2 = p2.m.get(i).getCoeficient();
			p3.m.add(new Monom(c2, e2));
		}
		return p3;
	}
	
	public Polinom multiply(Polinom p2) {
		Polinom p3 = new Polinom(x);
		for(int i=0;i<this.m.size();i++) {
			Monom m1 = this.m.get(i);
			for(int j=0;j<p2.m.size();j++) {
				Monom m2 = p2.m.get(j);
				Monom m3 = m1.multiply(m2);
				
				p3.m.add(m3);
			}
		}
	
	
		return p3;
	}
	
	
	
	public ArrayList<Polinom> divide(Polinom p4) {
		ArrayList<Polinom> p = new ArrayList<Polinom>();
		Polinom rez = new Polinom(x);
		Polinom rest = new Polinom(x);
		rest = this;
		if(rest.getGrad() < p4.getGrad()) {
			rez.m.add(new Monom(0,0));
		} else {
			
			while(rest.getGrad() >= p4.getGrad()) {
				Monom p1 = findMax(rest);
				Monom p2 = findMax(p4);
				Monom m = p1.divide(p2);
				Polinom poliDiv = new Polinom(m, x);
				rez.m.add(m);
				rest = rest.substract(p4.multiply(poliDiv));
		
			}
		}
		if(rest.m.isEmpty()) rest.m.add(new Monom(0,0));
		p.add(rez);
		p.add(rest);
		return p;
	}
	public Polinom substract(Polinom p2) {
		Polinom p3;
		for(int i=0;i<p2.m.size();i++) {
			p2.m.get(i).coef= p2.m.get(i).coef*(-1);
		}
		p3 = this.add(p2);
		p3.delete(); 
		return p3;
	}
	
	
}

