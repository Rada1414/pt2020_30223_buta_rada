import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Monom {
	public String monom;
	public int exponent;
	public int coef;
	public double doubleCoef;
	private String[] monom1;
	
	
	public Monom(String monom) {
		if(monomOk(monom))  {
			this.monom = monom;
		}
		doubleCoef = (double)coef;
	}
	
	public Monom(int coef, int exponent) {
		this.exponent = exponent;
		this.coef = coef;
		doubleCoef = coef;
		this.monom = format();
	}
	
	public Monom(double doubleCoef, int coef, int exponent) {
		this.exponent = exponent;
		this.coef = coef;
		this.doubleCoef = doubleCoef;
		this.monom = format();
	}
	
	private String format() {
		String txtFormat = new String(this.doubleCoef + "x^" + this.exponent);
		return txtFormat;
	}
	
	
	

	
	
	public String getMonom() {
		return (exponent == 0) ? Double.toString(doubleCoef) : (doubleCoef + "x^" + exponent);
	}
	
	public void afisare() {
		System.out.println("Putere: " + exponent + " Coeficient: " + coef + "\n");
	}
	
	public int getCoeficient() {
		return this.coef;
	}
	
	public int getExponent() {
		return this.exponent;
	}
	
	public int rez(int x) {
		return (int) (doubleCoef * Math.pow(x, exponent));
	}
	
	public double dValoare(double x) {
		return doubleCoef * Math.pow(x, exponent);
	}
	
	public void addCoef(int x) {
		this.coef += x;
		this.doubleCoef = (double)coef;
	}
	
	public Monom multiply(Monom mon1) {
		Monom ret;
		double c = this.doubleCoef * mon1.doubleCoef;
		int d = (int)c;
		int p = this.getExponent() + mon1.getExponent();
		ret = new Monom(c,d,p);
		return ret;
	}
	
	public Monom divide(Monom mon) {
		Monom m1;
		double coefNou = this.doubleCoef / mon.doubleCoef;
		int coefNouInt = (int)coefNou;
		int p = this.getExponent() - mon.getExponent();
		m1 = new Monom(coefNou, coefNouInt, p);
		return m1;
	}
	
	
	
	private boolean monomOk(String monom) {
		
		if(!monom.matches("^[a-zA-Z0-9\\^\\*\\- ]*")) return false;
		Pattern polyFormat = Pattern.compile("\\^");
		Matcher m = polyFormat.matcher(monom);
		String s = new String();
		while(m.find()) {
			s = m.group();
		}
		if(s.isEmpty()) {
		
			monom1 = monom.split("[a-zA-Z]");
			if(monom1.length == 0) {
				coef = 1;
				exponent = 1;
			} else {
				coef = (!monom1[0].isEmpty()) ? Integer.parseInt(monom1[0]) : 1;
				exponent = (monom1[0] == monom) ? 0 : 1;
			}
		} else {
			
			monom1 = monom.split("\\^"); 
			try {
				String nrStr = new String();
				

				for(int i = 0; i < monom1[0].length(); i++){
			        char c = monom1[0].charAt(i);
			        if(c==45) nrStr += c;
			        if(c > 47 && c < 58)nrStr += c;
			    }
				coef = (nrStr.isEmpty()) ? 1 : Integer.parseInt(nrStr);
				exponent = Integer.parseInt(monom1[1]);
			} catch(NumberFormatException e) { //     
				System.out.println("Formatul nu este valid");
			}
		} 
		return true;
	}
}

